var MongoClient = require('mongodb').MongoClient;
var config = require('../config');
var mongoose = require('mongoose');

var urlMongo = config.mongoDB.url + "/" + config.mongoDB.dbName;

module.exports = {
    dbConnection : function(callback){
        MongoClient.connect(urlMongo, function(err,db){
            callback(err,db);
        })
    }
}
