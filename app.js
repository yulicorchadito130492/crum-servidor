var config = require('./config');
var express= require('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));


app.listen(config.puerto,function(){
       console.log("Server listen at port 8001");
});

var consultaEndponit = require('./api/log_consulta');

app.use(config.apiVersion,consultaEndponit);
