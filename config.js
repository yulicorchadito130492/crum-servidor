module.exports = {
  puerto : 8001,
  apiVersion : "/api/",
  mongoDB :{
    ip : "localhost",
    url : "mongodb://localhost:27017",
    dbName : "logsCRUM"
  },
  HTTPcodes : {
    requestTimeout: 408,
    notFound: 404,
    unauthorized: 401,
    badRequest : 400,
    ok: 200,
    noContent: 204,
    internalServerError: 500,

  },
  consultaConnServer : 'logConnServer'
}
